from os import remove
from os import walk
from os.path import join


def cleanup():
    for root, dirs, files in walk('.'):
        for f in files:
            if f.lower().endswith('.pyc'):
                pth = join(root, f)
                print pth,
                try:
                    remove(join(root, f))
                    print 'deleted.'
                except (IOError, OSError) as e:
                    print e


if __name__ == '__main__':
    cleanup()
