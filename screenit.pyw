import wx

from lib.gui.main_frame import MainFrame
from os.path import basename


class SingleApp(wx.App):

    def OnInit(self):
        self.name = '{}-{}'.format(basename(__file__), wx.GetUserId())
        self.instance = wx.SingleInstanceChecker(self.name)
        if self.instance.IsAnotherRunning():
            return False
        return True


if __name__ == '__main__':
    app = SingleApp()
    frame = MainFrame(None)
    frame.Show(True)
    app.MainLoop()
