from lib.cmdline import CommandLine
from lib.imgproc import ImageProc
from lib.walker import collect_images

ARG_SPEC = [
    {
        'name': 'dpi',
        'convert': int,
        'assign': lambda v: v if v > 10 else 10,
        'value_required': True,
        'help_str': 'DPI. If not specified, the original value is used.',
    },
    {
        'name': 'width',
        'parse': lambda v: None if v is None else v.partition('cm'),
        'convert': lambda v: {
            'value': float(v[0]),
            'cm': bool(v[1]),
        },
        'value_required': True,
        'help_str': 'Width in pixels or centimeters (use prefix "cm").\n\t\t'
        'If not specified, the original value is used.',
    },
    {
        'name': 'centimeters',
        'cmd_arg': 'cm',
        'convert': float,
        'value_required': True,
        'help_str': 'Adjust DPI so that the image width fits\n\t\t'
        'the specified value in centimeters.',
    },
    {
        'name': 'auto_adjust',
        'assign': lambda v: True,
        'help_str': 'If specified along with the previous arg, DPI of only\n\t\t'
        'those images that are wider than "cm" value will be adjusted.',
    },
    {
        'name': 'frame',
        'assign': lambda v: v or '#3a67a6',
        'value_required': False,
        'help_str': 'Draw a frame of the specified color around an image.\n\t\t'
        'If a color is not specified, color #3a67a6 is used.',
    },
    {
        'name': 'recursive',
        'assign': lambda v: True,
        'help_str': 'Recursively process images in subdirs.',
    },
]


def main():
    cmdl = CommandLine(ARG_SPEC)
    if cmdl.empty:
        print cmdl.usage(
            '{paths to .png files or dirs}',
            'Args may be specified in any order.\n'
            'If no paths specified, .png files are processed starting from the current dir.\n'
            'Dir names are also parsed as command-line args and the files in such dirs are\n'
            'processed accordingly.'
        )
        return

    # collect files to process
    image_files = list(collect_images(cmdl))

    # process collected files
    print 'DETECTED IMAGES:\n\t%s\n' % (
        '\n\t'.join(f for f, _ in image_files) if image_files else 'None'
    )
    for file_name, options in image_files:
        with ImageProc(file_name) as img:
            if img is None:
                continue
            img.dpi = options.dpi.value
            img.width = options.width.value
            img.adjust_dpi(options.centimeters.value, options.auto_adjust.value)
            img.frame = options.frame.value


if __name__ == '__main__':
    main()
    raw_input('Press ENTER to exit... ')
