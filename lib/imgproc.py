from PIL import Image
from PIL import ImageDraw
from common import get_copy
from functools import partial


class ImageProc(object):

    def __init__(self, file_name, copy=True, discard_original=False):
        self.name = file_name
        try:
            self.image = Image.open(get_copy(file_name, discard_original) if copy else file_name)
        except IOError:
            print 'Cannot identify image file "{}".'.format(file_name)
            self.image = None

    def __enter__(self):
        if self.image is not None:
            return self

    def __exit__(self, exc_typ, exc_val, tbk):
        self.save()

    def save(self):
        if self.image is None:
            return
        try:
            self.image.save(self.name, **self.image.info)
        except IOError:
            print 'Cannot save "{}".'.format(self.name)

    @property
    def dpi(self):
        return self.image.info.get('dpi', (None, None))[0]

    @dpi.setter
    def dpi(self, value):
        if value is None:
            return
        orig_dpi = self.dpi
        if orig_dpi is None:
            print 'Cannot change DPI in "{}".'.format(self.name)
            return
        if orig_dpi == value:
            return
        print '{} - changing DPI: {} -> {}...'.format(self.name, orig_dpi, value)
        self.image.info['dpi'] = value, value

    @property
    def width(self):
        return self.image.size[0]

    @width.setter
    def width(self, data):
        if data is None:
            return
        width = int(data['value'])
        dpi_info = ''
        if data['cm']:
            dpi = self.dpi
            if dpi is None:
                print 'Cannot change width in cm. No DPI in "{}".'.format(self.name)
                return
            width = int(round(data['value'] * dpi / 2.54))
            dpi_info = '({:.2f} cm based on {} DPI)'.format(data['value'], dpi)

        orig_width, orig_height = self.image.size
        if orig_width == width:
            return
        wpercent = (width / float(orig_width))
        height = int((float(orig_height) * float(wpercent)))
        print '{} - changing width: {}, {} -> {}, {}... {}'.format(
            self.name, orig_width, orig_height, width, height, dpi_info
        )
        self.image = self.image.resize((width, height), Image.ANTIALIAS)

    @property
    def height(self):
        return self.image.size[1]

    @property
    def frame(self):
        return

    @frame.setter
    def frame(self, color):
        if color is None:
            return
        print '{} - drawing frame...'.format(self.name)
        draw = ImageDraw.Draw(self.image)
        line = partial(draw.line, fill=color, width=1)
        w, h = self.width - 1, self.height - 1
        line([(0, 0), (w, 0)])
        line([(w, 0), (w, h)])
        line([(w, h), (0, h)])
        line([(0, h), (0, 0)])
        del draw

    def __adjust_required(self, centimeters):
        dpi = self.dpi
        if dpi is None:
            print 'Cannot auto-adjust DPI in "{}".'.format(self.name)
            return
        max_width = int(round(centimeters * dpi / 2.54))
        if self.width > max_width:
            return True
        else:
            print 'Width of "{}" is {} cm ({} DPI). DPI auto-adjust is not required.'.format(
                self.name, round(2.54 * self.width / dpi, 2), dpi
            )

    def adjust_dpi(self, centimeters, auto):
        if centimeters is None:
            return
        if auto and not self.__adjust_required(centimeters):
            return
        print '{} - DPI will be adjusted so that image width fits {:.2f} cm.'.format(
            self.name, centimeters
        )
        self.dpi = int(round(2.54 * self.width / centimeters))
