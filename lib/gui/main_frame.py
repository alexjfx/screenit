import sys
import wx

from ..common import async
from ..common import cmd
from ..common import float_to_int
from ..common import make_dirs
from ..imgproc import ImageProc
from ..registry import Registry
from PIL.ImageGrab import grabclipboard
from __frames__ import __MainFrame__
from __icon__ import AppIcon
from consts import *
from desktop import DEFAULT_DPI
from desktop import DesktopFrame
from os.path import dirname
from os.path import exists
from saveable import SaveableFrameMixin
from tray import TrayIcon
from wx import BITMAP_TYPE_PNG
from wx import Bitmap
from wx import CallAfter
from wx import EVT_CHECKBOX
from wx import TheClipboard


def redirect_to_textCtrl(control_name):

    class __Redirect:

        def __init__(self, textCtrl):
            self.out = textCtrl

        def write(self, string):
            CallAfter(self.out.AppendText, string)

    def decorator(func):
        def wrapper(*args, **kwargs):
            sys.stdout = __Redirect(getattr(args[0], control_name))
            sys.stderr = sys.stdout
            res = func(*args, **kwargs)
            sys.stdout = sys.__stdout__
            sys.stderr = sys.__stderr__
            return res
        return wrapper
    return decorator


class MainFrame(__MainFrame__, SaveableFrameMixin):

    file_counter = 1

    def __init__(self, parent):
        __MainFrame__.__init__(self, parent)
        self.SetIcon(AppIcon.GetIcon())
        self.tray = TrayIcon(self)
        self.__IconizeOnClose__ = True

        self.reg = reg = Registry('SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run')
        for name, ctrl in self.__dict__.iteritems():
            if name.endswith('Btn'):
                pth = DATA_DIR + name[:-3] + '.png'
                if exists(pth):
                    ctrl.SetBitmap(Bitmap(pth))
                continue

            if not name.endswith('Cbx'):
                continue
            bind_obj_name = name[:-3] + 'Ctrl'
            bind_obj = getattr(self, bind_obj_name, None)
            if bind_obj is None:
                ctrl.bind_obj = None
                continue
            ctrl.bind_obj = bind_obj
            ctrl.Bind(EVT_CHECKBOX, self.__OnCheckBox)

        SaveableFrameMixin.__init__(self, SETTINGS_DB, {
            'tplCombo': '',
            'noteBook': (0, 'SetSelection', 'GetSelection'),
            'dpiCbx': False,
            'widthCbx': False,
            'cmCbx': False,
            'adCbx': False,
            'frameCbx': False,
            'originalCbx': True,
            'editorCbx': False,
            'clipCbx': False,
            'dpiCtrl': 96,
            'widthCtrl': 300,
            'cmCtrl': 16,
            'frameCtrl': ('#3a67a6', 'SetColour', 'GetColour'),
            'dirCtrl': (dirname(sys.argv[0]), 'SetPath', 'GetPath', make_dirs, None),
            'editorCtrl': '',
            'colorCtrl': (wx.RED, 'SetColour', 'GetColour'),
            'stepCtrl': 5,
            'wdCtrl': 2,
            WINDOW_POS: None,
            WINDOW_MIN: False,
            TEMPLATES: {},
        }, {'logCtrl'})

        self.autoStartCheck.SetValue(reg.exists(self.GetTitle()))
        self.tplCombo.AppendItems(self.settings[TEMPLATES].keys())
        self.ValidateControls()

        self.SetWindowPos()
        if self.settings[WINDOW_MIN]:
            self.Iconize()

        self.autoStartCheck.Bind(EVT_CHECKBOX, self.__OnAutoCheck)
        self.fileBtn.Bind(wx.EVT_BUTTON, self.ScreenshotFromFile)
        self.clipBtn.Bind(wx.EVT_BUTTON, self.ScreenshotFromClipboard)
        self.screenBtn.Bind(wx.EVT_BUTTON, self.TakeScreenshot)
        self.saveBtn.Bind(wx.EVT_BUTTON, self.__OnSaveTemplate)
        self.delBtn.Bind(wx.EVT_BUTTON, self.__OnDelTemplate)
        self.tplCombo.Bind(wx.EVT_TEXT_ENTER, self.__OnSaveTemplate)
        self.tplCombo.Bind(wx.EVT_COMBOBOX, self.ChangeTemplate)
        self.Bind(wx.EVT_ICONIZE, self.__OnIconize)
        self.Bind(wx.EVT_CLOSE, self.__OnClose)

    def OnLoad(self):
        for name, ctrl in self.controls.iteritems():
            if not name.endswith('Cbx'):
                continue
            try:
                ctrl.obj.bind_obj.Enable(ctrl.obj.IsChecked())
            except AttributeError:
                pass

    def SetWindowPos(self):
        wp = self.settings[WINDOW_POS]
        if wp is None:
            self.Center()
            return
        dw, dh = wx.DisplaySize()
        ww, wh = self.GetSize()
        wt, wl = wp
        self.SetPosition((
            dw - ww - 100 if wt + ww > dw else wt,
            dh - wh - 100 if wl + wh > dh else wl
        ))

    @property
    def ImageProcRequired(self):
        return (
            self.dpiCbx.IsChecked() or
            self.widthCbx.IsChecked() or
            self.cmCbx.IsChecked() or
            self.frameCbx.IsChecked()
        )

    def ValidateControls(self):
        cm_checked = self.cmCbx.IsChecked()
        self.adCbx.Enable(cm_checked)
        self.fileBtn.Enable(self.ImageProcRequired)

    def edit(self, path):
        if not self.editorCbx.IsChecked():
            return
        editor = self.editorCtrl.GetPath().strip()
        if not exists(editor):
            editor = 'mspaint'
        cmd('{editor} "{path}"', editor=editor, path=path, hide_wnd=False)

    def copy_to_clipboard(self, path):
        if not self.clipCbx.IsChecked():
            return
        if not TheClipboard.Open():
            print 'Cannot copy to clipboard.\n\n'
            return

        TheClipboard.SetData(wx.BitmapDataObject(Bitmap(path, BITMAP_TYPE_PNG)))
        TheClipboard.Flush()
        TheClipboard.Close()

    @async
    @redirect_to_textCtrl('logCtrl')
    def ProcessImage(self, path, discard_original=True):
        self.edit(path)

        if not self.ImageProcRequired:
            CallAfter(self.copy_to_clipboard, path)
            return

        with ImageProc(path, self.originalCbx.IsChecked(), discard_original) as img:
            if img is None:
                return
            if self.dpiCbx.IsChecked():
                img.dpi = float_to_int(self.dpiCtrl.GetValue())
            if self.widthCbx.IsChecked():
                img.width = {
                    'value': float_to_int(self.widthCtrl.GetValue()),
                    'cm': False,
                }
            if self.cmCbx.IsChecked():
                img.adjust_dpi(self.cmCtrl.GetValue(), self.adCbx.IsChecked())
            if self.frameCbx.IsChecked():
                img.frame = self.frameCtrl.GetColour().Get()
            print 'Done.\n'

        CallAfter(self.copy_to_clipboard, path)

    def SaveImage(self, image, parent=None):
        if image is None:
            return

        with wx.FileDialog(
            parent or self,
            'Select a file',
            self.dirCtrl.GetPath(),
            'Screenshot #{}'.format(self.file_counter),
            'PNG files (*.png)|*.png',
            wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT
        ) as dlg:
            if dlg.ShowModal() == wx.ID_CANCEL:
                return image.close()
            self.dirCtrl.SetPath(dlg.GetDirectory())
            path = dlg.GetPath()

        try:
            image.save(path, 'PNG', dpi=(DEFAULT_DPI, DEFAULT_DPI))
            self.file_counter += 1
        except IOError:
            self.logCtrl.AppendText('Cannot save "{}".\n\n'.format(path))
        finally:
            image.close()

        if exists(path):
            self.ProcessImage(path)

    def __OnSaveTemplate(self, event):
        event.Skip()
        combo = self.tplCombo
        st = combo.GetValue().strip()
        if not st:
            return
        idx = combo.FindString(st)
        if idx == wx.NOT_FOUND:
            combo.Append(st)
            actual_st = st
        else:
            actual_st = combo.GetString(idx)
            combo.SetValue(actual_st)
        self.Save()
        self.settings[TEMPLATES][actual_st] = dict(self.settings.public_items())

    def __OnDelTemplate(self, event):
        event.Skip()
        combo = self.tplCombo
        st = combo.GetValue().strip()
        if not st:
            return
        idx = combo.FindString(st)
        if idx == wx.NOT_FOUND:
            return
        actual_st = combo.GetString(idx)
        combo.Delete(idx)
        del self.settings[TEMPLATES][actual_st]

    def ChangeTemplate(self, event):
        if isinstance(event, (str, unicode)):
            name = event
        else:
            name = self.tplCombo.GetValue().strip()
            event.Skip()

        self.settings.update(self.settings[TEMPLATES][name])
        self.Load()
        self.ValidateControls()

    def __OnCheckBox(self, event):
        event.GetEventObject().bind_obj.Enable(event.IsChecked())
        self.ValidateControls()
        event.Skip()

    def __OnIconize(self, event):
        self.Hide()
        event.Skip()

    def __OnClose(self, event):
        if self.__IconizeOnClose__:
            self.Iconize()
            return

        self.Save({
            WINDOW_POS: self.GetPosition(),
            WINDOW_MIN: self.IsIconized(),
        })
        self.tray.RemoveIcon()
        self.tray.Destroy()
        self.reg.close()
        event.Skip()

    def __OnAutoCheck(self, event):
        if event.IsChecked():
            self.reg.set(self.GetTitle(), sys.argv[0])
        else:
            self.reg.delete(self.GetTitle())
        event.Skip()

    def ScreenshotFromClipboard(self, event):
        self.SaveImage(grabclipboard())
        event.Skip()

    def ScreenshotFromFile(self, event):
        self.tray.RemoveIcon()
        with wx.FileDialog(
            self,
            'Select a file',
            self.dirCtrl.GetPath(),
            '',
            'PNG files (*.png)|*.png',
            wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        ) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                self.ProcessImage(dlg.GetPath(), self.editorCbx.IsChecked())
        self.tray.ShowIcon()
        event.Skip()

    def TakeScreenshot(self, event):
        self.Iconize()
        DesktopFrame(
            self,
            step=float_to_int(self.stepCtrl.GetValue()),
            pen_color=self.colorCtrl.GetColour(),
            pen_width=float_to_int(self.wdCtrl.GetValue()),
            image_func=self.SaveImage,
        ).Show(True)
        event.Skip()
