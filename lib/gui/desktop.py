import wx

from PIL.ImageGrab import grab
from wx import Frame

MASK_COLOR = wx.BLACK
DEFAULT_DPI = 96


class __RectFrame__(Frame):

    def __init__(self, parent, pen_color, pen_width):
        Frame.__init__(
            self, parent, size=(0, 0),
            style=wx.STAY_ON_TOP | wx.FRAME_NO_TASKBAR | wx.FRAME_SHAPED | wx.NO_BORDER,
        )
        self.SetDoubleBuffered(True)
        self.Disable()
        self.SetBackgroundColour(pen_color)

        self.pw = pen_width
        self.pwh = int(round(float(pen_width) / 2))
        self.txtdist = pen_width * 2 + 10
        self.txtframe = self.txtdist + 200

        self.__dc = dc = wx.MemoryDC()
        dc.SetBackground(wx.Brush(MASK_COLOR))
        dc.SetBrush(wx.TRANSPARENT_BRUSH)
        dc.SetPen(wx.Pen(pen_color, pen_width))
        dc.SetTextForeground(pen_color)

        self.Show(True)

    def move(self, x, y):
        pw = self.pw
        self.SetPosition((x - pw, y - pw))

    def resize(self, top, left, width, height):
        if not (width and height):
            self.SetSize((0, 0))
            return

        txtframe = self.txtframe
        txtdist = self.txtdist
        pw = self.pw
        pwh = self.pwh
        info = '{pxw}px x {pxh}px\n{cmw:.2f}cm x {cmh:.2f}cm\n({dpi} dpi)'.format(
            pxw=width,
            pxh=height,
            cmw=width * 2.54 / DEFAULT_DPI,
            cmh=height * 2.54 / DEFAULT_DPI,
            dpi=DEFAULT_DPI
        )

        dc = self.__dc
        bitmap = wx.EmptyBitmap(width + txtframe, height + txtframe)
        try:
            dc.SelectObject(bitmap)
            dc.DrawRectangle(pw - pwh, pw - pwh, width + pw + 1, height + pw + 1)
            dc.DrawText(info, width + txtdist, height + txtdist)
            dc.SelectObject(wx.NullBitmap)
            bitmap.SetMaskColour(MASK_COLOR)
            self.SetRect((top - pw, left - pw, width + txtframe, height + txtframe))
            self.SetShape(wx.RegionFromBitmap(bitmap))
        finally:
            del bitmap

        self.Refresh()
        self.Update()


class DesktopFrame(Frame):

    origin = None
    diff = None
    rect = 0, 0, 0, 0

    def __init__(self, parent, step=1, pen_color=wx.RED, pen_width=1, image_func=lambda x: None):
        Frame.__init__(
            self, parent, style=wx.FRAME_NO_TASKBAR | wx.MAXIMIZE | wx.STAY_ON_TOP
        )
        self.SetCursor(wx.StockCursor(wx.CURSOR_CROSS))
        self.SetDoubleBuffered(True)
        self.SetTransparent(1)
        self.Raise()

        self.rect_frame = __RectFrame__(self, pen_color, pen_width)
        self.step = step
        self.image_func = image_func
        self.__configurable = False

        set_evt = self.Bind
        set_evt(wx.EVT_LEFT_DOWN, self.__OnMouseLeftDown)
        set_evt(wx.EVT_RIGHT_DOWN, self.__OnMouseRightDown)
        set_evt(wx.EVT_MOTION, self.__OnMouseMove)
        set_evt(wx.EVT_LEFT_UP, self.__OnMouseLeftUp)
        set_evt(wx.EVT_CHAR_HOOK, self.__OnCharHook)
        set_evt(wx.EVT_CLOSE, self.__OnClose)

    @property
    def empty_rect(self):
        return not (self.rect[2] and self.rect[3])

    def __OnMouseLeftDown(self, event):
        self.origin = event.GetPosition()

    def __OnMouseRightDown(self, event):
        self.diff = None
        if not self.__configurable or self.empty_rect:
            return
        top, left, width, height = self.rect
        x, y = event.GetPosition()
        if not (top <= x <= top + width and left <= y <= left + height):
            return
        self.diff = x - top, y - left

    def __OnMouseMove(self, event):
        if not event.Dragging():
            return

        if event.LeftIsDown():
            step = self.step
            x1, y1 = self.origin
            x2, y2 = event.GetPosition()
            cx, cy = ((x2 - x1) // step) * step, ((y2 - y1) // step) * step
            self.rect = rect = min(x1, x1 + cx), min(y1, y1 + cy), abs(cx), abs(cy)
            self.rect_frame.resize(*rect)
        elif event.RightIsDown() and self.__configurable and self.diff is not None:
            x, y = event.GetPosition()
            dx, dy = self.diff
            cx, cy = x - dx, y - dy
            _, _, w, h = self.rect
            self.rect = cx, cy, w, h
            self.rect_frame.move(cx, cy)

    def __OnMouseLeftUp(self, event=None):
        if self.empty_rect or self.__configurable:
            return
        top, left, width, height = self.rect
        self.image_func(grab((top, left, top + width, left + height)), self)
        self.Close()

    def __OnCharHook(self, event):
        key = event.GetKeyCode()
        if event.GetKeyCode() == wx.WXK_ESCAPE:
            self.Close()
        if key == wx.WXK_SPACE:
            if not self.__configurable:
                self.__configurable = True
            elif not (self.empty_rect or wx.GetMouseState().LeftIsDown()):
                self.__configurable = False
                self.__OnMouseLeftUp()

    def __OnClose(self, event):
        self.rect_frame.Close()
        event.Skip()
