import wx

from PIL.ImageGrab import grabclipboard
from __icon__ import AppIcon
from consts import *
from wx import NewId

CLIPBOARD_MENUITEM_ID = NewId()
FILE_MENUITEM_ID = NewId()
SETTINGS_MENUITEM_ID = NewId()
EXIT_MENUITEM_ID = NewId()


class TrayMenu(wx.Menu):

    def __init__(self, frame):
        wx.Menu.__init__(self)
        self.frame = frame

        item = self.AddItem
        separator = self.AppendSeparator

        item(
            CLIPBOARD_MENUITEM_ID, 'From clipboard', frame.ScreenshotFromClipboard,
            enable=grabclipboard() is not None,
        )
        item(
            FILE_MENUITEM_ID, 'From .png file', frame.ScreenshotFromFile,
            enable=frame.ImageProcRequired,
        )
        item(SETTINGS_MENUITEM_ID, 'Settings', self.__OnSettingsSelection)
        separator()

        templates = frame.settings[TEMPLATES]
        if templates:
            tpl = frame.tplCombo.GetValue().strip()
            curtpl = tpl if tpl in templates else None
            for tpl in sorted(templates):
                item(NewId(), tpl, self.__OnTemplateSelection, highlight=tpl == curtpl)
            separator()

        item(EXIT_MENUITEM_ID, 'Exit', self.__OnExitSelection)

    def AddItem(self, id, label, handler, enable=True, highlight=False):
        item = wx.MenuItem(self, id, label)
        if highlight:
            font = item.GetFont()
            font.SetWeight(wx.BOLD)
            item.SetFont(font)
        item.Enable(enable)
        self.AppendItem(item)
        self.Bind(wx.EVT_MENU, handler, id=id)

    def __OnSettingsSelection(self, event):
        frame = self.frame
        iconized = frame.IsIconized()
        shown = frame.IsShown()
        frame.Iconize(not iconized)
        frame.Show(not shown)
        if frame.IsShown():
            frame.Raise()
        event.Skip()

    def __OnTemplateSelection(self, event):
        self.frame.ChangeTemplate(self.GetLabel(event.GetId()))
        event.Skip()

    def __OnExitSelection(self, event):
        frame = self.frame
        frame.__IconizeOnClose__ = False
        frame.Close()
        event.Skip()


class TrayIcon(wx.TaskBarIcon):

    def __init__(self, frame):
        wx.TaskBarIcon.__init__(self)
        self.frame = frame
        self.ShowIcon()

        self.Bind(wx.EVT_TASKBAR_LEFT_DOWN, frame.TakeScreenshot)
        self.Bind(wx.EVT_TASKBAR_CLICK, self.__OnClick)

    def ShowIcon(self):
        self.SetIcon(AppIcon.GetIcon(), self.frame.GetTitle())

    def __OnClick(self, event):
        self.PopupMenu(TrayMenu(self.frame))
