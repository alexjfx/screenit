from os.path import exists
from shutil import copyfile
from wx.tools.img2py import img2py

ICON_PYFILE = '__icon__.py'

if __name__ == '__main__':
    copyfile('app.ico', '..\\..\\app.ico')
    img2py('app.ico', ICON_PYFILE, imgName='AppIcon')
    if exists(ICON_PYFILE):
        with open(ICON_PYFILE, 'r+') as fd:
            content = fd.readlines()
            fd.seek(0)
            fd.truncate()
            fd.writelines(content[3:-1])
