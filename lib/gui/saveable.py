from ..common import SaveableDict


class Control(object):

    def __init__(self, obj, default=None, setter=None, getter=None,
                 sval_fn=None, gval_fn=None):
        self.obj = obj
        self.default = default
        self.setter = setter
        self.getter = getter
        self.sval_fn = sval_fn
        self.gval_fn = gval_fn


class SaveableFrameMixin:

    def __init__(self, file_path, spec=None, ignore=None):
        ignore = ignore or set()
        self.controls = controls = {
            name: Control(
                ctrl,
                None,
                getattr(ctrl, 'SetValue', None),
                getattr(ctrl, 'GetValue', None),
                lambda x: x,
                lambda x: x,
            )
            for name, ctrl in self.__dict__.iteritems()
            if name not in ignore
        }

        spec = spec or {}
        other_defaults = {}
        for name, data in spec.iteritems():
            if name[0] == '_':
                other_defaults[name] = data
                continue

            ctrl = controls[name]

            if not isinstance(data, (tuple, list)):
                ctrl.default = data
                continue

            sval_fn, gval_fn = None, None
            if len(data) == 3:
                default, setter, getter = data
            else:
                default, setter, getter, sval_fn, gval_fn = data
            ctrl.default = default
            ctrl.setter = getattr(ctrl.obj, setter, None)
            ctrl.getter = getattr(ctrl.obj, getter, None)
            if sval_fn is not None:
                ctrl.sval_fn = sval_fn
            if gval_fn is not None:
                ctrl.gval_fn = gval_fn

        defaults = {name: ctrl.default for name, ctrl in controls.iteritems()}
        defaults.update(other_defaults)
        self.settings = SaveableDict(file_path, defaults)
        self.Load()

    def Load(self):
        settings = self.settings
        for name, ctrl in self.controls.iteritems():
            if ctrl.setter is None:
                continue
            try:
                ctrl.setter(ctrl.sval_fn(settings[name]))
            except TypeError:
                pass
        self.OnLoad()

    def Save(self, other=None):
        settings = {
            name: ctrl.gval_fn(ctrl.getter())
            for name, ctrl in self.controls.iteritems()
            if ctrl.getter is not None
        }
        if other is not None:
            settings.update(other)
        self.settings.update(settings)
        self.OnSave()

    def OnLoad(self):
        pass

    def OnSave(self):
        pass
