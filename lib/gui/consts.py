import sys

from os.path import dirname


DATA_DIR = dirname(sys.argv[0]) + '/data/'
SETTINGS_DB = DATA_DIR + 'settings.pkl'

WINDOW_POS = '__fpos__'
WINDOW_MIN = '__icon__'
TEMPLATES = '__templates__'
