from os.path import basename
from sys import argv


class Argument(object):

    def __init__(self, name, parse=None, convert=None, assign=None, value_required=None,
                 optional=True, help_str='', cmd_arg='', order_id=None):
        self.__idx__ = order_id
        self.__name__ = name

        self.cmd_arg = cmd_arg if cmd_arg else name[0]
        self.value_required = value_required
        self.optional = optional
        self.help_str = help_str

        self.__parse = parse
        self.__convert = convert
        self.__assign = assign
        self.__value = None

    def __repr__(self):
        required = self.value_required
        if required is None:
            st = self.cmd_arg
        elif required is True:
            st = '{}=value{}'.format(self.cmd_arg, self.__idx__)
        else:
            st = '{}[=value{}]'.format(self.cmd_arg, self.__idx__)
        return '{{{}}}'.format(st) if self.optional else st

    def __str__(self):
        return '{arg}\t{optional}{help}'.format(
            arg=self.cmd_arg,
            optional='[Optional] ' if self.optional else '',
            help=self.help_str
        )

    @property
    def value(self):
        return self.__value

    @value.setter
    def value(self, val):
        val = val if self.__parse is None else self.__parse(val)

        if self.__convert is not None:
            try:
                val = self.__convert(val)
            except (ValueError, TypeError):
                print 'Cannot convert value "{}".'.format(val)
                return

        self.__value = val if self.__assign is None else self.__assign(val)


class CommandLine(object):

    def __init__(self, arg_spec, arg_list=argv):
        self.__spec = arg_spec
        self.__app = basename(arg_list[0])
        self.__tail = tail = set()

        argmap = {}
        for idx, spec in enumerate(arg_spec, 1):
            arg = Argument(order_id=idx, **spec)
            name = arg.__name__
            self[name] = arg
            argmap[arg.cmd_arg] = name

        args = (arg.split('=', 1) if '=' in arg else (arg, None) for arg in arg_list[1:])
        for cmd_arg, val in args:
            if cmd_arg in argmap:
                self[argmap[cmd_arg]].value = val
            else:
                tail.add(cmd_arg)

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def __delitem__(self, key):
        del self.__dict__[key]

    def __iter__(self):
        for value in sorted(
            (v for k, v in self.__dict__.iteritems() if k[0] != '_'),
            key=lambda v: v.__idx__
        ):
            yield value

    @property
    def arg_spec(self):
        return self.__spec

    @property
    def app_name(self):
        return self.__app

    @property
    def tail(self):
        return self.__tail

    @property
    def empty(self):
        for arg in self:
            if arg.value is not None:
                return False
        return True

    def usage(self, tail_info, info):
        return 'Usage: {app} {args} {tail}\nWhere:\n\t{usage}\n\n{info}'.format(
            app=self.app_name,
            args=' '.join(repr(v) for v in self),
            tail=tail_info,
            usage='\n\t'.join(str(v) for v in self),
            info=info,
        )
