from cPickle import dump
from cPickle import load
from os import makedirs
from os import utime
from os.path import dirname
from os.path import exists
from os.path import splitext
from shutil import copy2
from subprocess import CREATE_NEW_CONSOLE
from subprocess import CalledProcessError
from subprocess import STARTF_USESHOWWINDOW
from subprocess import STARTUPINFO
from subprocess import STDOUT
from subprocess import call
from subprocess import check_output
from threading import Thread

float_to_int = lambda x: int(round(x))


class SaveableDict(dict):

    def __init__(self, file_path, elements={}):
        dict.__init__(self, elements)
        self.__file = file_path
        try:
            with open(touch(file_path), 'rb') as fd:
                dict.update(self, load(fd))
        except EOFError:
            pass
        except (OSError, IOError) as e:
            print 'Cannot load data: %s' % e

    def save(self):
        try:
            with open(self.__file, 'wb') as fd:
                dump(self, fd)
        except (OSError, IOError) as e:
            print 'Cannot save data: %s' % e

    def update(self, elements):
        dict.update(self, elements)
        self.save()

    def public_items(self):
        for k, v in self.iteritems():
            if k[0] != '_':
                yield k, v

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        self.save()

    def __delitem__(self, key):
        dict.__delitem__(self, key)
        self.save()


class Async(Thread):

    def __init__(self, func, *args, **kwargs):
        name = kwargs.pop('name', None)
        daemon = kwargs.pop('daemon', False)
        Thread.__init__(self, target=func, name=name, args=args, kwargs=kwargs)
        self.daemon = daemon
        self.start()


def async(func):
    def wrapper(*args, **kwargs):
        return Async(func, *args, **kwargs)
    return wrapper


def get_copy(source_file, discard_original=False):
    print 'Creating a copy of "{}" if not exists...'.format(source_file)
    fname, ext = splitext(source_file)
    copy_file = fname + '_original' + ext
    if exists(copy_file) and not discard_original:
        return copy_file
    try:
        copy2(source_file, copy_file)
        return copy_file
    except IOError as e:
        print 'Cannot create a copy of "{}". {}'.format(source_file, e)


def make_dirs(pth):
    try:
        if not exists(pth):
            makedirs(pth)
    except OSError:
        pass
    return pth


def touch(file_path):
    try:
        # recreate dir structure
        pth = dirname(file_path)
        if pth and not exists(pth):
            makedirs(pth)
        # create file if not exists and touch it
        with open(file_path, 'a'):
            utime(file_path, None)
    except (OSError, IOError) as e:
        print 'Cannot touch: %s' % e
    return file_path


def cmd(command, **kwargs):
    kpop = kwargs.pop
    output = kpop('output', False)
    err2out = kpop('err2out', False)
    hide_wnd = kpop('hide_wnd', True)
    cwd = kpop('cwd', None)
    new_console = kpop('new_console', False)
    cl = command.format(**kwargs)
    startupinfo = None
    if hide_wnd:
        startupinfo = STARTUPINFO()
        startupinfo.dwFlags |= STARTF_USESHOWWINDOW
    crf = CREATE_NEW_CONSOLE if new_console else 0
    try:
        if output:
            return check_output(
                cl,
                cwd=cwd,
                startupinfo=startupinfo,
                creationflags=crf,
                stderr=STDOUT if err2out else None
            )
        else:
            return not bool(call(cl, cwd=cwd, startupinfo=startupinfo, creationflags=crf))
    except OSError as e:
        print 'Command "%s" failed: %s' % (cl, e)
        return '' if output else False
    except CalledProcessError as e:
        return e.output
