from cmdline import CommandLine
from os.path import basename
from os.path import isdir
from os.path import isfile
from os.path import join
from scandir import walk


def collect_images(cmdl):
    recursive = cmdl.recursive.value
    for pth in cmdl.tail or {'.'}:
        if isfile(pth):
            yield pth, cmdl
        elif isdir(pth):
            for root, _, files in walk(pth):
                # try to parse dir name
                dir_cmdl = CommandLine(cmdl.arg_spec, [''] + basename(root).split())
                options = cmdl if dir_cmdl.empty else dir_cmdl
                for f in (st.lower() for st in files):
                    if f.endswith('.png') and not f.endswith('_original.png'):
                        yield join(root, f), options
                if recursive is None:
                    break
        else:
            print '"{}" is skipped because it is not a file or directory.'.format(pth)
